<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('loan_application_id')->unsigned()->unique();
            $table->foreign('loan_application_id')->references('id')->on('loan_applications')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->decimal('amount')->comment('Load Amount');
            $table->integer('term')->comment('Terms in week');
            $table->decimal('rate_of_interest')->comment('Loan Rate of Interest');
            $table->decimal('emi')->nullable()->comment('EMI Per Week');
            $table->integer('repayments')->default(0)->comment('Number of repayments so far');
            $table->tinyInteger('status')->nullable()->comment('NULL = Open, 1 = Close, 2 = Default');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
};
