<?php

namespace App\Observers;

use App\Models\Loan;
use App\Models\LoanRepayment;
use Carbon\Carbon;

class LoanObserver
{
    /**
     * Handle the Loan "created" event.
     *
     * @param \App\Models\Loan $loan
     * @return void
     */
    public function created(Loan $loan)
    {
        //Let's create loan repayment schedule once loan is approved
        $repayment = 0;
        $interval  = 7;
        for ($i = 0; $i < $loan->term; $i++)
        {
            $repayment = $repayment + $loan->emi;
            LoanRepayment::create(
                [
                    'loan_id'          => $loan->id,
                    'amount'           => (float)$loan->emi,
                    'remaining_amount' => (float)$loan->amount - $repayment,
                    'due_date'         => Carbon::now()->addDays($interval),
                ]
            );
            $interval = $interval + 7;
        }
    }

    /**
     * Handle the Loan "updated" event.
     *
     * @param \App\Models\Loan $loan
     * @return void
     */
    public function updated(Loan $loan)
    {
        //
    }

    /**
     * Handle the Loan "deleted" event.
     *
     * @param \App\Models\Loan $loan
     * @return void
     */
    public function deleted(Loan $loan)
    {
        //
    }

    /**
     * Handle the Loan "restored" event.
     *
     * @param \App\Models\Loan $loan
     * @return void
     */
    public function restored(Loan $loan)
    {
        //
    }

    /**
     * Handle the Loan "force deleted" event.
     *
     * @param \App\Models\Loan $loan
     * @return void
     */
    public function forceDeleted(Loan $loan)
    {
        //
    }
}
