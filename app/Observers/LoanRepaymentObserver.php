<?php

namespace App\Observers;

use App\Models\Loan;
use App\Models\LoanRepayment;

class LoanRepaymentObserver
{
    /**
     * Handle the LoanRepayment "created" event.
     *
     * @param \App\Models\LoanRepayment $loanRepayment
     * @return void
     */
    public function created(LoanRepayment $loanRepayment)
    {

    }

    /**
     * Handle the LoanRepayment "updated" event.
     *
     * @param \App\Models\LoanRepayment $loanRepayment
     * @return void
     */
    public function updated(LoanRepayment $loanRepayment)
    {
        $remaining_payment_count = LoanRepayment::whereLoanId($loanRepayment->loan_id)->whereStatus(null)->count();
        if ($remaining_payment_count == 0)
        {
            Loan::whereId($loanRepayment->loan_id)->update(['status' => 1]);

            //Todo: we can also send here notification as well for loan is closed
        }
    }

    /**
     * Handle the LoanRepayment "deleted" event.
     *
     * @param \App\Models\LoanRepayment $loanRepayment
     * @return void
     */
    public function deleted(LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Handle the LoanRepayment "restored" event.
     *
     * @param \App\Models\LoanRepayment $loanRepayment
     * @return void
     */
    public function restored(LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Handle the LoanRepayment "force deleted" event.
     *
     * @param \App\Models\LoanRepayment $loanRepayment
     * @return void
     */
    public function forceDeleted(LoanRepayment $loanRepayment)
    {
        //
    }
}
