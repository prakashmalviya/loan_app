<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 *
 */
class SendLoanApprovalEmailToCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $loan;
    public $subject;

    public function __construct($loan, $subject)
    {
        $this->loan    = $loan;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->subject)
            ->view('emails.loan-approval');
    }
}
