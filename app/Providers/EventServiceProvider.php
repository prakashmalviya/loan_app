<?php

namespace App\Providers;

use App\Events\LoanApprove;
use App\Listeners\LoanApproveNotification;
use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Observers\LoanObserver;
use App\Observers\LoanRepaymentObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class  => [SendEmailVerificationNotification::class,],
        LoanApprove::class => [LoanApproveNotification::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Loan::observe(LoanObserver::class);
        LoanRepayment::observe(LoanRepaymentObserver::class);
    }
}
