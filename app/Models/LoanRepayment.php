<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\LoanRepayment
 *
 * @property int $id
 * @property int $loan_id
 * @property int $amount Amount Pay
 * @property int $remaining_amount Loan Remaining Amount
 * @property int|null $status NULL = UnPaid, 1 = Paid
 * @property string $due_date Due date of EMI
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Loan $loan
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment newQuery()
 * @method static \Illuminate\Database\Query\Builder|LoanRepayment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereLoanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereRemainingAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LoanRepayment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|LoanRepayment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|LoanRepayment withoutTrashed()
 * @mixin \Eloquent
 */
class LoanRepayment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'loan_id',
        'amount',
        'remaining_amount',
        'due_date',
        'status',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }
}
