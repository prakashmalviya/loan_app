<?php

namespace App\Listeners;

use App\Events\LoanApprove;
use App\Mail\SendLoanApprovalEmailToCustomer;
use Log;
use Mail;

class LoanApproveNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\LoanApprove $event
     * @return void
     */
    public function handle(LoanApprove $event)
    {
        try
        {
            $subject = trans('loan.approve', ['id' => $event->loan->id]);
            Mail::to($event->loan->user->email)->send(new SendLoanApprovalEmailToCustomer($event->loan, $subject));
        }
        catch (\Exception $e)
        {
            Log::info('There is problem with loan application email sent: ' . $e->getMessage());
        }
    }
}
