<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class AuthController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|AuthResource
     */
    public function registration(Request $request): \Illuminate\Http\JsonResponse|AuthResource
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:8',
            'name'     => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try
        {
            $user = User::create([
                'name'     => $request->get('name'),
                'email'    => $request->get('email'),
                'password' => $request->get('password'),
                'role'     => 'user',
            ]);

            return new AuthResource($user);

        }
        catch (\Exception $e)
        {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')]))
        {
            $token = auth()->user()->createToken('MyAppToken')->plainTextToken;
            return response()->json(['token' => $token], 200);
        }
        else
        {
            return response()->json(['error' => 'Unauthorised'], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json(['success' => trans('auth.logout')], Response::HTTP_OK);
    }

}
