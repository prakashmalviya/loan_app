<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\LoanResource;
use App\Models\Loan;
use App\Models\LoanRepayment;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class LoanController extends Controller
{
    /**
     * @param Request $request
     * @return LoanResource|\Illuminate\Http\JsonResponse|void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function emiPay(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'loan_id' => 'required|exists:loans,id,user_id,' . auth()->id()
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $loan_id = $request->get('loan_id');
        $loan    = Loan::whereId($loan_id)->first();

        $this->authorize('view', $loan);

        if ($loan->status == null)
        {
            $loan_repayment = LoanRepayment::whereLoanId($loan_id)->whereStatus(null)->orderBy('id')->first();
            if ($loan_repayment)
            {
                $loan_repayment->status = 1;
                $loan_repayment->save();

                $loan->repayments = $loan->repayments + 1;
                $loan->save();
            }
            return new LoanResource(Loan::whereId($loan_id)->first());

        }
        elseif ($loan->status == 1)
        {
            return response()->json(['error' => trans('loan.closed')], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        elseif ($loan->status == 2)
        {
            return response()->json(['error' => trans('loan.default')], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
