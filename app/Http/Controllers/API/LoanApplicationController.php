<?php

namespace App\Http\Controllers\API;

use App\Events\LoanApprove;
use App\Http\Controllers\Controller;
use App\Http\Resources\LoanApplicationCollection;
use App\Http\Resources\LoanApplicationResource;
use App\Http\Resources\LoanResource;
use App\Models\Loan;
use App\Models\LoanApplication;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class LoanApplicationController extends Controller
{
    /**
     * @return LoanApplicationCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(): LoanApplicationCollection
    {
        $this->authorize('viewAny', LoanApplication::class);
        return new LoanApplicationCollection(LoanApplication::with('user')->paginate());
    }

    /**
     * @param Request $request
     * @return LoanApplicationResource|\Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse|LoanApplicationResource
    {
        $this->authorize('create', LoanApplication::class);

        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'term'   => 'required|numeric',
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try
        {
            $loan_application = LoanApplication::create([
                'user_id' => auth()->id(),
                'amount'  => $request->get('amount'),
                'term'    => $request->get('term'),
            ]);
            return new LoanApplicationResource(LoanApplication::with('user')->whereId($loan_application->id)->first());
        }
        catch (\Exception $e)
        {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return LoanApplicationResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function view($id, Request $request): LoanApplicationResource
    {
        $this->authorize('view', LoanApplication::class);

        return new LoanApplicationResource(LoanApplication::whereId($id)->first());
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function approve(Request $request)
    {
        $this->authorize('viewAny', LoanApplication::class);

        $validator = Validator::make($request->all(), [
            'application_id'   => 'required|numeric|exists:loan_applications,id',
            'rate_of_interest' => 'required|numeric',
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $loan_application = LoanApplication::whereId($request->get('application_id'))->first();

        if ($loan_application->status == null)
        {
            $loan = Loan::create(
                [
                    'loan_application_id' => $loan_application->id,
                    'user_id'             => $loan_application->user_id,
                    'amount'              => $loan_application->amount,
                    'term'                => $loan_application->term,
                    'emi'                 => (float)$loan_application->amount / $loan_application->term,
                    'rate_of_interest'    => $request->get('rate_of_interest')
                ]
            );

            LoanApprove::dispatch($loan);

            $loan_application->status = 1;
            $loan_application->save();

            return new LoanResource($loan);

        }
        elseif ($loan_application->status == 1)
        {
            return response()->json(['error' => trans('loan.already_proved')], Response::HTTP_BAD_REQUEST);
        }
        elseif ($loan_application->status == 2)
        {
            return response()->json(['error' => trans('loan.rejected')], Response::HTTP_BAD_REQUEST);
        }
    }

}
