<?php

namespace App\Http\Resources;

use App\Models\LoanApplication;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

/**
 * @mixin LoanApplication
 */
class LoanApplicationResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    #[Pure] #[ArrayShape(['id' => "int", 'amount' => "string", 'term' => "int", 'created_at' => "\Illuminate\Support\Carbon|null"])]
    public function toArray($request): array
    {
        return [
            'id'         => $this->id,
            'amount'     => $this->amount,
            'term'       => $this->term,
            'status'     => $this->status,
            'created_at' => $this->created_at,
            'user'       => new UserResource($this->user),
        ];
    }
}
