<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"                  => $this->id,
            "loan_application_id" => $this->loan_application_id,
            "user_id"             => $this->user_id,
            "amount"              => $this->amount,
            "term"                => $this->term,
            "rate_of_interest"    => $this->rate_of_interest,
            "emi"                 => $this->emi,
            "repayments"          => $this->repayments,
            "status"              => $this->status,
        ];
    }
}
