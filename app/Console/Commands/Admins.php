<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class Admins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admins:add {--email=} {--password=} {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command helps you to create admins for platform.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getArguments()
    {
        return [
            ['email'],
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->option('email');
        if (!$email)
        {
            $email = $this->ask("Input admin's email, please.");
        }

        $password = $this->option('password');
        if (!$password)
        {
            $password = $this->ask("Input admin's password, please.");
        }

        $name = $this->option('name');
        if (!$name)
        {
            $name = $this->ask("Input admin's name, please.");
        }

        $this->line("Email: $email, password: $password, name: $name");
        if (!$this->confirm('Do you wish to continue? [y|N]'))
        {
            return;
        }

        $result = User::create([
            'name'     => $name,
            'email'    => $email,
            'password' => $password,
            'role'     => 'admin',
        ]);

        if (!$result)
        {
            $this->error("Admin not created, something went wrong...");
        }
        else
        {
            $this->info("Admin created successfully.");
        }
    }
}
