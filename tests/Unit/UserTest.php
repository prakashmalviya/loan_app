<?php

namespace Tests\Unit;

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function test_registration()
    {
        $faker        = Faker::create();
        $password     = $faker->password(8);
        $user_payload = [
            'name'     => $faker->firstName,
            'email'    => $faker->email,
            'password' => $password
        ];

        $this->json('post', route('registration'), $user_payload)
             ->assertStatus(ResponseAlias::HTTP_CREATED)
             ->assertJsonStructure(
                 [
                     'data' => [
                         'id',
                         'name',
                         'email',
                         'created_at'
                     ]
                 ]
             )->json();

        unset($user_payload['password']);
        $this->assertDatabaseHas('users', $user_payload);

    }

    /**
     * @return void
     */
    public function test_login()
    {
        $faker        = Faker::create();
        $password     = $faker->password(8);
        $user_payload = [
            'name'     => $faker->firstName,
            'email'    => $faker->email,
            'password' => $password
        ];

        $this->json('post', route('registration'), $user_payload)
             ->assertStatus(ResponseAlias::HTTP_CREATED)
             ->assertJsonStructure(
                 [
                     'data' => [
                         'id',
                         'name',
                         'email',
                         'created_at'
                     ]
                 ]
             )->json();

//        $login_payload = [
//            'email'    => $faker->email,
//            'password' => $password
//        ];
//
//        $response = $this->json('post',route('login'), $login_payload)
//             ->assertStatus(ResponseAlias::HTTP_OK);
//dd($response);
//        //$this->assertDatabaseHas('users', $login_payload);

        if (Auth::attempt(['email' => $user_payload['email'], 'password' => $password]))
        {
            $this->assertTrue(true);
        }
    }
}
