<?php

namespace Tests\Unit;

use App\Models\Loan;
use App\Models\LoanApplication;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\NoReturn;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class LoanRepaymentTest extends TestCase
{
    use WithoutMiddleware, DatabaseTransactions;

    /**
     * @return void
     */
    #[NoReturn] public function test_emi_pay()
    {
        $faker        = Faker::create();
        $password     = $faker->password(8);
        $user_payload = [
            'name'     => $faker->firstName,
            'email'    => $faker->email,
            'password' => $password
        ];

        $user = $this->json('post', route('registration'), $user_payload)
                     ->assertStatus(ResponseAlias::HTTP_CREATED)
                     ->assertJsonStructure(
                         [
                             'data' => [
                                 'id',
                                 'name',
                                 'email',
                                 'created_at'
                             ]
                         ]
                     )->json();

        $loan_application = LoanApplication::whereStatus(null)->first();

        if ($loan_application)
        {
            $loan = Loan::create(
                [
                    'loan_application_id' => $loan_application->id,
                    'user_id'             => $user['data']['id'],
                    'amount'              => $loan_application->amount,
                    'term'                => $loan_application->term,
                    'emi'                 => (float)$loan_application->amount / $loan_application->term,
                    'rate_of_interest'    => $faker->randomFloat(2, 1, 12)
                ]
            );

            if (Auth::attempt(['email' => $user_payload['email'], 'password' => $password]))
            {
                $this->json('post', route('payEmi'), [
                    'load_id' => $loan->id
                ])->assertStatus(ResponseAlias::HTTP_CREATED)->json();
            }
        }
    }
}
