<?php

namespace Tests\Unit;

use App\Models\LoanApplication;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class LoanApplicationTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_loan_application()
    {
        $faker        = Faker::create();
        $password     = $faker->password(8);
        $user_payload = [
            'name'     => $faker->firstName,
            'email'    => $faker->email,
            'password' => $password
        ];

        $this->json('post', route('registration'), $user_payload)
             ->assertStatus(ResponseAlias::HTTP_CREATED)
             ->assertJsonStructure(
                 [
                     'data' => [
                         'id',
                         'name',
                         'email',
                         'created_at'
                     ]
                 ]
             )->json();

        unset($user_payload['password']);
        $this->assertDatabaseHas('users', $user_payload);

        if (Auth::attempt(['email' => $user_payload['email'], 'password' => $password]))
        {
            $this->assertTrue(true);

            $loan_application_payload = [
                'amount' => $faker->numberBetween(1000, 100000),
                'term'   => $faker->numberBetween(1, 260),
            ];

            $loan_application = $this->json('post', route('addApplication'), $loan_application_payload)
                                     ->assertStatus(ResponseAlias::HTTP_OK)
                                     ->assertJsonStructure(
                                         [
                                             'data' => [
                                                 'id',
                                                 'amount',
                                                 'term',
                                                 'created_at',
                                             ]
                                         ]
                                     )->json();
            unset($loan_application['data']['user']);
            $this->assertDatabaseHas('loan_applications', $loan_application['data']);
            session('loan_application_id', $loan_application['data']['id']);
        }

    }

    public function test_get_all_application()
    {
        $faker        = Faker::create();
        $password     = $faker->password(8);
        $user_payload = [
            'name'     => $faker->firstName,
            'email'    => $faker->email,
            'password' => $password,
            'role'     => 'admin',
        ];

        $user = User::create($user_payload);

        if (Auth::attempt(['email' => $user->email, 'password' => $password]))
        {
            $this->assertTrue(true);

            $this->json('get', route('getAllApplication'))
                 ->assertStatus(ResponseAlias::HTTP_OK)->json();
        }

    }

    public function test_approve_loan_application()
    {
        $faker        = Faker::create();
        $password     = $faker->password(8);
        $user_payload = [
            'name'     => $faker->firstName,
            'email'    => $faker->email,
            'password' => $password,
            'role'     => 'admin',
        ];

        $user = User::create($user_payload);

        if (Auth::attempt(['email' => $user->email, 'password' => $password]))
        {
            $loan_applications = LoanApplication::whereStatus(null)->first();

            if ($loan_applications)
            {
                $this->json('post', route('approveApplication'), [
                    'application_id'   => $loan_applications->id,
                    'rate_of_interest' => $faker->randomFloat(2, 1, 12),
                ])->assertStatus(ResponseAlias::HTTP_CREATED)->json();

            }
        }
        $this->assertTrue(true);

    }
}
