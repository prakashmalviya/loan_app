<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Loan Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during loan application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'application'    => [
        'success' => 'Your registration is successful',
        'error'   => 'There is problem with your loan application, Please try again later.',
    ],
    'approve'        => 'Congratulation! Your load request is approved. Load Id :id',
    'already_proved' => 'Requested loan is already approved',
    'rejected'       => 'Requested loan is rejected, Please process with new application.',
    'closed'         => 'This loan is already closed.',
    'default'        => 'This loan is defaulted, contact branch for repayments',

];
