<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\LoanApplicationController;
use App\Http\Controllers\API\LoanController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//API Version v1
Route::group(['prefix' => 'v1'], function () {

    //Public Routes
    Route::post('registration', [AuthController::class, 'registration'])->name('registration');
    Route::post('login', [AuthController::class, 'login'])->name('login');

    //Authenticated Routes
    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::post('logout', [AuthController::class, 'logout'])->name('logout');

        Route::group(['prefix' => 'loan-application'], function () {
            Route::get('/', [LoanApplicationController::class, 'index'])->name('getAllApplication');
            Route::get('{id}', [LoanApplicationController::class, 'show'])->name('getApplication');
            Route::post('store', [LoanApplicationController::class, 'store'])->name('addApplication');
            Route::post('approve/loan', [LoanApplicationController::class, 'approve'])->name('approveApplication');
        });

        Route::group(['prefix' => 'loan'], function () {
            Route::post('emi/pay', [LoanController::class, 'emiPay'])->name('payEmi');
        });
    });
});
